#include "Helper.h"
#include <Windows.h>
#include <iostream>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <direct.h>
#include <filesystem>
#include <experimental/filesystem>
#include <TlHelp32.h>
#include <cstdio>
HANDLE ghMutex;

DWORD WINAPI WriteToDatabase(LPVOID);
namespace fs = std::experimental::filesystem;
typedef unsigned int(WINAPI* avVersion)(void);
#define BUFSIZE MAX_PATH
void currp()
{
	TCHAR pwd[MAX_PATH];
	GetCurrentDirectory(MAX_PATH, pwd);
	MessageBox(NULL, pwd, pwd, 0); // show msgbox
	std::cout << "Your location >> " << pwd << std::endl;
}
int APIENTRY WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
	// call the imported function found in the dll
	//int result = IsolatedFunction("hello", "world");

	return 0;
}
int main()
{
	
	TCHAR Buffer[BUFSIZE];
	DWORD dwRet;
	std::cout << "#############################################################" << std::endl;
	std::cout << "#                    _                                      #" << std::endl;
	std::cout << "#                  -=\\`\\                                    #" << std::endl;
	std::cout << "#              |\\ ____\\_\\__                                 #" << std::endl;
	std::cout << "#            -=\\c`""        "  """"""     "` )                               #" << std::endl;
	std::cout << "#               `~~~~~/ /~~`\                                #" << std::endl;
	std::cout << "#                 -==/ /                                    #" << std::endl;
	std::cout << "#                   '-'                                     #" << std::endl;
	std::cout << "#                  _  _                                     #" << std::endl;
	std::cout << "#                 ( `   )_                                  #" << std::endl;
	std::cout << "#                (    )    `)                               #" << std::endl;
	std::cout << "#              (_   (_ .  _) _)                             #" << std::endl;
	std::cout << "#                                             _             #" << std::endl;
	std::cout << "#                                            (  )           #" << std::endl;
	std::cout << "#             _ .                         ( `  ) . )        #" << std::endl;
	std::cout << "#           (  _ )_                      (_, _(  ,_)_)      #" << std::endl;
	std::cout << "#         (_  _(_ ,)      FUN ONLY  tomy.p                  #" << std::endl;
	std::cout << "#############################################################" << std::endl;
	while (true)
	{
		std::cout << ">>";
		std::string com;
		std::getline(std::cin, com);
		std::vector<std::string> Command = Helper::get_words(com);
		if (Command[0] == "pwd")
		{
			currp();

		}
		else if (Command[0] == "cd")
		{

			if (!SetCurrentDirectory(Command[1].c_str()))
			{
				printf("SetCurrentDirectory failed (%d)\n", GetLastError());
			}
			std::cout << "Your new location now is : " << std::endl;
			currp();

		}
		else if (Command[0] == "create")
		{
			try
			{
				CreateFile(Command[1].c_str(), GENERIC_READ, 0, NULL, CREATE_NEW, FILE_ATTRIBUTE_NORMAL, NULL);
			}
			catch (...)
			{
				std::cout << "Error while create file try again later" << std::endl;
			}
		}
		else if (Command[0] == "ls")
		{
			TCHAR pwd[MAX_PATH];
			GetCurrentDirectory(MAX_PATH, pwd);
			std::string path = pwd;
			for (auto & p : fs::directory_iterator(path))
				std::cout << p << std::endl;
		}
		else if (Command[0] == "secret")
		{
			HINSTANCE LoadMe = LoadLibrary("C:/Users/magshimim/source/repos/18_tomypoli/18_tomypoli/Secret.dll");
			if (NULL != LoadMe)
			{
				avVersion func = (avVersion)GetProcAddress(LoadMe, "TheAnswerToLifeTheUniverseAndEverything");
				if (NULL != func)
				{
					unsigned int result = func();
					std::cout << "dll output >> - " << result << std::endl;
				}
				else
				{
					std::cout << "couldnt open function" << std::endl;
				}
			}
			else
			{
				std::cout << "couldnt open dll" << std::endl;
			}

		}
		else if (Command[0] == "run")
		{
			STARTUPINFO info={sizeof(info)};
			PROCESS_INFORMATION processInfo;
			LPCSTR file = Command[1].c_str();
			LPSTR cmd = const_cast<char *>(Command[2].c_str());
			if (CreateProcess(file, cmd, NULL, NULL, TRUE, 0, NULL, NULL, &info, &processInfo))
			{
				WaitForSingleObject(processInfo.hProcess, INFINITE);
				CloseHandle(processInfo.hProcess);
				CloseHandle(processInfo.hThread);
			}
		}
	}
	system("PAUSE");
}
